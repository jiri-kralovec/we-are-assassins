<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
            <?php
            get_template_part('partials/sidebar/sidebar','postcategories');
            ?>
            </div>
            <div class="col-xs-12 col-md-offset-1 col-md-8">
            <?php

            global $wp_query;
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $catId = $wp_query->get_queried_object_id();

            echo '<h1>'.get_cat_name($catId).'</h1>';

            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 10,
                'paged' => $paged,
                'nopaging' => false,
                'cat' => $catId
            );
            $query = new WP_Query($args);
            if($query->have_posts()){
                while($query->have_posts()){
                    $query->the_post();

                    $posted = get_the_date('j/n/Y g:i', $post->ID);
                    $excerpt = substr(get_the_content(), 0, 200) . '... <a href="'.get_the_permalink().'" title="'.get_the_title().'">Read more</a>';

                    echo '
                    <article class="postfeed__post">
                        <div class="row">
                            <div class="col-sm-3 col-md-4 hidden-xs">
                                <a class="postfeed__postthumbnail" href="'.get_the_permalink().'" title="'.get_the_title().'" style="background-image: url(\''.get_the_post_thumbnail_url().'\');"></a>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-8">
                                <a class="postfeed__postheadline" href="'.get_the_permalink().'" title="'.get_the_title().'">
                                    <h2>'.get_the_title().'</h2>
                                </a>
                                <span class="postfeed__postauthor">'.get_the_author().'</span>
                                <span class="postfeed__posttime">'.$posted.'</span>
                                <p class="postfeed__postexcerpt">'.$excerpt.'</p>
                            </div>
                        </div>
                    </article>
                    ';
                }

                get_page_pagination($query);
            } else {
                echo '<p>It looks like no posts have been published yet. Please come back later.</p>';
            }
            ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'community');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>