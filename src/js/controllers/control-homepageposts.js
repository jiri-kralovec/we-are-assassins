$(document).ready(function(){
    $('.js-homepageremposts').on('click', function(){
        /* Change sidebar state */
        $('.js-homepageremposts.active').removeClass('active');
        $(this).addClass('active');

        /* Change displayed content */
        var wrapper = $('.homepage__remainingposts');
        wrapper.find('.feed--active').removeClass('feed--active');

        var target = '.'+$(this).attr('data-bind');
        $(target).addClass('feed--active');
    });
});