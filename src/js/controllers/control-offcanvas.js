$(document).ready(function(){
    $('.js-trigger').on('click', function(){    
        var target = $(this).attr('data-target');
        switch(target){
            case 'offcanvas__profile':
                $('.offcanvas__navigation').hide('slide', 300);
                break;
            case 'offcanvas__navigation':
                $('.offcanvas__profile').hide('slide', 300);
        }

        $('.'+target).delay(100).toggle('slide',350);
    });
    /* Block document scroll if offcanvas is active */
    var offcanvasChecker = setInterval(function(){
        if($(".offcanvas").is(":visible")){
            $('html, body').css('overflowY','hidden');
        } else {
            $('html, body').css('overflowY','auto');
        }
    },100);
});