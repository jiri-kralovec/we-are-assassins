$(document).ready(function(){
    var operational = true;

    $('.js-selectposttype').on('click', function(){
        if(!operational){
            return false;
        }

        /* Set animation var */
        operational = false;

        /* Negate active class */
        var parent = $(this).parent();
        parent.find('.active').removeClass('active');
        $(this).addClass('active');

        /* Hide active posttype form */
        $('.newpost__postform.active').toggle(300, function(){
            $(this).removeClass('active');
        });

        /* Show new posttype form */
        var active = $(this).attr('data-reference');
        $('section[data-reference="'+active+'"]').toggle(300, function(){
            $(this).addClass('active');
            operational = true;
        });
    });

    $('.js-fileinput').on('click', function(){
        var target = 'input#'+$(this).attr('data-reference');
        $(target).click();
    });
});