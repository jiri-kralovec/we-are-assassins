$(document).ready(function(){
    var actualSlide = 1;
    var maxSlides = $('.js-featuredpost').length - 1;
    var slideTime = 4500;

    /* Functions */
    function progressBarAnimation(){
        /* Reset progress bar */
        $('.loading-bar-progress').css({width:0});
        $('.loading-bar-progress').animate({
            width: '100%'
        }, slideTime, 'swing', function(){
            progressBarAnimation();
        });
    }
    function replaceFeaturedContent(raw, manual){
        if(manual){
            clearInterval(autoplay);
        }

        var target = $('.featured__post');
        var element = raw.find('span');
        var img = raw.find('img').attr('src');

        /* Adjust wrapping link */
        $('.homepage__featuredposts').find('a').attr('href',element.attr('data-permalink')).attr('title',element.attr('data-title'));
        
        /* Adjust background image */
        target.attr("style", "background-image: url('"+img+"')");

        /* Change texts */
        target.find('h1.title').text(element.attr('data-title'));
        target.find('span.author').text(element.attr('data-author'));
        target.find('span.time').text(element.attr('data-posttime'));
    }

    /* Handlers */
    $('.js-featuredpost').on({
        /*
        mouseenter: function(){
            $('.loading-bar').remove();
            $('.js-featuredpost.active').removeClass('active');
            $(this).addClass('active');
            replaceFeaturedContent($(this));
        },
        */
        click: function(){
            $('.loading-bar').remove();
            $('.js-featuredpost.active').removeClass('active');
            $(this).addClass('active');
            replaceFeaturedContent($(this));
        }
    });

    /* Loop and animations */
    progressBarAnimation();
    $('.js-featuredpost').eq(0).addClass('active');
    
    var autoplay = setInterval(function(){
        var target = $('.js-featuredpost').eq(actualSlide);
    
        $('.js-featuredpost.active').removeClass('active');
        target.addClass('active');
        replaceFeaturedContent(target);

        if(actualSlide == maxSlides){
            actualSlide = 0;
        } else {
            actualSlide++;
        }
    },slideTime);
});