<div style="max-width: 560px; padding: 20px; background: #ffffff; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 3px #eeeeee;">We Are Assassins</div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Your account has been deleted and all personal data are no longer stored in our database.</div>
</div>
<div style="color: #999; padding: 20px 30px;">Best regards,<br />We Are Assassins Team</div>
</div>