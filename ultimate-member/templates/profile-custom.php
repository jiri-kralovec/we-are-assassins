<?php /* Template: Lightweight profile */ ?>
<?php
$profile_id = um_profile_id();
um_fetch_user($profile_id);

$formError = false;

if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "change_avatar"){
    /* validation */
    if(!$_FILES || $_FILES['imagecontent']['error'] > UPLOAD_ERR_OK){
        $formError = true;
    }
    if($formError === false){
        /* Create post */
        $user = wp_get_current_user()->ID;
        $currentAvatar = get_avatar_url($user);

        foreach($_FILES as $file => $array){
            break;

            $filename = $array['name'];
            $filetype = basename($filename);
            $filedata = $array['tmp_name'];



            $upload_file = wp_upload_bits($filename, null, file_get_contents($filedata));
            if(!$upload_file['error']){
                $wp_filetype = wp_check_filetype($filetype, null);
            }

            break;
        }
    }
}
?>

<?php
    $profile_id = um_profile_id();
    um_fetch_user($profile_id);
?>

<div class="col-xs-12 usersection__profileheader">
    <?php
    $avatarUri = um_get_user_avatar_url();
    $name = um_get_display_name($profile_id);
    
    if(um_is_myprofile()){
        echo '
            <img class="userprofile__avatar js-changeprofile-photo" data-reference="changeprofile_photo" alt="'.$name.'" src="'.$avatarUri.'">
            <h2 class="userprofile__name">'.$name.'</h2>

            <span style="display: none;">
        ';
        get_template_part('partials/forms/form', 'change_avatar');
        echo '
            </span>
        ';
    } else {
        echo '
            <img class="userprofile__avatar" alt="'.$name.'" src="'.$avatarUri.'">
            <h2 class="userprofile__name">'.$name.'</h2>
        ';
    }
    ?>
</div>
<div class="col-xs-12 col-sm-6 usersection__postfeed">
    <header>
        <h2 class="postfeed__headline">Latest posts</h2>
        <?php
        if(um_is_myprofile()){
            echo '<a class="btn btn--default btn--primary postfeed__addnew" title="Add post" href="/novy-prispevek/">Add post</a>';
        }
        ?>
    </header>
<?php
    $args = array(
        'post_type'     => array('waa_post','waa_imagepost'),
        'posts_per_page'=> -1,
        'author'        => $profile_id
    );
    $query = new WP_Query($args);
    if($query->have_posts()){
        while($query->have_posts()){
            $query->the_post();
            $posted = get_the_date('j/n/Y g:i', $post->ID);
            $content;

            switch(get_post_type()){
                case 'waa_post':
                    $content = '<p class="post__plain">'.substr(get_the_content(), 0, 200) . '...</p>';
                    break;
                case 'waa_imagepost':
                    $content = get_the_content();
            }

            echo '
            <a href="'.get_the_permalink().'" title="Příspěvek uživatele '.$user.'">
                <article class="postfeed__post">
                    <div class="post__userimage">
                        <img class="post__avatar" src="'.$avatarUri.'" alt="'.$user.'">
                    </div>
                    <div class="post__body">
                        <span class="post__date">'.$posted.'</span>
                        <span class="post__content">'.$content.'</span>
                    </div>
                </article>
            </a>
            ';
        }
    } else {
        if(um_is_myprofile()){
            echo '<p>Your feed is empty! Click the button above and start publishing now!</p>';
        } else {
            echo '<p>'.$name.' haven\'t published anything yet.</p>';
        }
    }
?>
</div>
<div class="col-xs-12 col-sm-6 usersection__editprofile">
    <header>
        <?php
        if(um_is_myprofile()){
            echo '<h2>Edit profile</h2>';
        } else {
            echo  '<h2>About the user</h2>';
        }
        ?>
    </header>
    <?php
    if(um_is_myprofile()){
        echo do_shortcode('[ultimatemember_account]');
    } else {
        $user = get_userdata($profile_id);
        $bio = get_user_meta($user->ID, 'description', true);
        
        echo '
            <h3 class="userprofile__label">Name</h3>
            <span class="userprofile__name">'.$name.'</span>
            <h3 class="userprofile__label">About</h3>
            <p>'.$bio.'</p>
        ';
    }
    ?>
</div>