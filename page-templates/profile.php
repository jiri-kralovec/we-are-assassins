<?php
/*
Template Name: Profile page
*/
?>

<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content page__userprofile">
    <div class="container">
        <div class="row">
            <?php
                $profile_id = um_profile_id();
                um_fetch_user($profile_id);
            ?>

            <div class="col-xs-12 usersection__profileheader">
                <?php
                $avatarUri = um_get_user_avatar_url();
                $name = um_get_display_name($profile_id);
                
                if(um_is_myprofile()){
                    echo '
                        <img class="userprofile__avatar js-changeprofile-photo" data-reference="changeprofile_photo" alt="'.$name.'" src="'.$avatarUri.'">
                        <h2 class="userprofile__name">'.$name.'</h2>

                        <span style="display: none;">
                    ';
                    get_template_part('partials/forms/form', 'change_avatar');
                    echo '
                        </span>
                    ';
                } else {
                    echo '
                        <img class="userprofile__avatar" alt="'.$name.'" src="'.$avatarUri.'">
                        <h2 class="userprofile__name">'.$name.'</h2>
                    ';
                }
                ?>
            </div>
            <div class="col-xs-12 col-sm-6 usersection__postfeed">
                <header>
                    <h2 class="postfeed__headline">Latest posts</h2>
                    <?php
                    if(um_is_myprofile()){
                        echo '<a class="btn btn--default btn--primary postfeed__addnew" title="Add post" href="/novy-prispevek/">Add post</a>';
                    }
                    ?>
                </header>
            <?php
                $args = array(
                    'post_type'     => array('waa_post','waa_imagepost'),
                    'posts_per_page'=> -1,
                    'author'        => $profile_id
                );
                $query = new WP_Query($args);
                if($query->have_posts()){
                    while($query->have_posts()){
                        $query->the_post();
                        $posted = get_the_date('j/n/Y g:i', $post->ID);
                        $content;

                        switch(get_post_type()){
                            case 'waa_post':
                                $content = '<p class="post__plain">'.substr(get_the_content(), 0, 200) . '...</p>';
                                break;
                            case 'waa_imagepost':
                                $content = get_the_content();
                        }

                        echo '
                        <a href="'.get_the_permalink().'" title="Posted by '.$user.'">
                            <article class="postfeed__post">
                                <div class="post__userimage">
                                    <img class="post__avatar" src="'.$avatarUri.'" alt="'.$user.'">
                                </div>
                                <div class="post__body">
                                    <span class="post__date">'.$posted.'</span>
                                    <span class="post__content">'.$content.'</span>
                                </div>
                            </article>
                        </a>
                        ';
                    }
                } else {
                    if(um_is_myprofile()){
                        echo '<p>Your feed is empty! Click the button above and start publishing now!</p>';
                    } else {
                        echo '<p>'.$name.' haven\'t published anything yet.</p>';
                    }
                }
            ?>
            </div>
            <div class="col-xs-12 col-sm-6 usersection__editprofile">
                <section class="userprofile__fields">
                    <header>
                        <?php
                        if(um_is_myprofile()){
                            echo '<h2>Edit profile</h2>';
                        } else {
                            echo  '<h2>About '.$name.'</h2>';
                        }
                        ?>
                    </header>
                    <?php
                    if(um_is_myprofile()){  
                        get_template_part('partials/forms/form','edit_profile');
                    ?>
                    <?php
                    } else {
                        $user = get_userdata($profile_id);
                        $bio = get_user_meta($user->ID, 'description', true);
                        
                        echo '
                            <h3 class="userprofile__label">Name</h3>
                            <span class="userprofile__name">'.$name.'</span>
                            <h3 class="userprofile__label">Bio</h3>
                            <p>'.$bio.'</p>
                        ';
                    }
                    ?>
                </section>
                <?php
                // Password change
                if(um_is_myprofile()){
                ?>
                <section class="userprofile__fields">
                    <header>
                        <h2>Change password</h2>
                    </header>
                    <?php
                        get_template_part('partials/forms/form','change_password');
                    ?>
                </section>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>