<?php
/*
Template Name: New post
*/
?>

<?php
/* Redirect logged-in users  */
if(!is_user_logged_in()){
    header('Location: /prihlasit-se/', true, 302);
    die();
}

get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<?php
    $activeTab = null;

    if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )){
        switch($_POST['action']){
            case 'new_text_post':
                $activeTab = 'waa_post';
                break;
            case 'new_image_post':
                $activeTab = 'waa_imagepost';
        }
    }
?>

<div class="page__content page__newpost">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Create new post</h1>
                <section class="newpost__posttype">
                    <header>
                        <h2>Select post type</h2>
                    </header>
                    <?php
                    $args = array(
                        'public' => true,
                        '_builtin' => false
                    );
                    $output = 'name';
                    $operator = 'and';
                    $types = get_post_types($args, $output, $operator);

                    if($types){
                        echo '<ul class="posttype__list">';
                        foreach($types as $type){
                            $isActive = ($activeTab == $type->name) ? ' active' : '';
                            echo '<li class="js-selectposttype'.$isActive.'" data-reference="'.$type->name.'">'.$type->labels->add_new.'</li>';
                        }
                        echo '</ul>';
                    } else {
                        echo 'Communityt posts are not currently supported.';
                    }
                    ?>
                </section>
                <?php
                if($types){
                    foreach($types as $type){
                        $isActive = ($activeTab == $type->name) ? ' active' : '';
                        echo '
                            <section class="newpost__postform'.$isActive.'" data-reference="'.$type->name.'">
                                <header>
                                    <h2>'.$type->labels->add_new.'</h2>
                                </header>
                        ';
                        get_template_part('partials/forms/form', $type->name);
                        echo '</section>';
                    }
                }
                ?>
            </div>
            <div class="col-xs-12">
            <?php
            get_template_part('partials/feeds/feed', 'latestnews');
            ?>
            </div>
            <div class="col-xs-12">
            <?php
            get_template_part('partials/feeds/feed', 'community');
            ?>
            </div>
        </div>    
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>