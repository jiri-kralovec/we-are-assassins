<?php
/*
Template Name: Login
*/
?>

<?php
/* Redirect logged-in users  */
if(is_user_logged_in()){
    header('Location: /profil/', true, 302);
    die();
}

get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content page__userprofile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                    if(have_posts()){
                        while(have_posts()){
                            the_post();
                            echo '<h1>'.get_the_title().'</h1>';
                            echo '<div class="userprofiles__form userprofiles__loginform">';
                            the_content();
                            echo '</div>';
                        }
                    }
                ?>
            </div>
            <style type="text/css">
                .um-notice{
                    display: none !important;
                }
            </style>
            <div class="col-xs-12 hidden-desktop">
                <?php
                get_template_part('partials/feeds/feed', 'latestnews');
                ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'community');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>