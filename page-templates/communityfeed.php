<?php
/*
Template Name: Community feed
*/
?>

<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Posted by community</h1>
            </div>

            <div class="col-xs-12">
                <div class="row community-feed">
                    <?php

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $args = array(
                        'post_type'     => array('waa_post','waa_imagepost'),
                        'posts_per_page'=> 12,
                        'paged' => $paged,
                        'nopaging' => false
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()){
                        while($query->have_posts()){
                            $query->the_post();
                            $posted = get_the_date('j/n/Y g:i', $post->ID);
                            $content;
                            
                            um_fetch_user(get_the_author_meta('ID'));

                            $avatarUrl = um_get_user_avatar_url();
                
                            switch(get_post_type()){
                                case 'waa_post':
                                    $content = '<p class="post__plain">'.substr(get_the_content(), 0, 200) . '...</p>';
                                    break;
                                case 'waa_imagepost':
                                    $content = get_the_content();
                            }
                
                            echo '
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a href="'.get_the_permalink().'" title="Posted by '.get_the_author().'">
                                        <article class="post">
                                            <div class="post__userimage">
                                                <img class="post__avatar" src="'.$avatarUrl.'" alt="'.get_the_author().'">
                                            </div>
                                            <div class="post__body">
                                                <span class="post__content">'.$content.'</span>
                                                <span class="post__date">'.$posted.'</span>
                                            </div>
                                        </article>
                                    </a>   
                                </div>
                            ';
                        }
                        get_page_pagination($query, array('older' => 'Starší příspěvky', 'newer' => 'Novější příspěvky'));
                    } else {
                        if(is_user_logged_in()){
                            echo '<p>Nikdo z komunity zatím nepublikoval žádný příspěvek. <a href="/novy-prispevek/" title="Přidat článek">Buď první!</a></p>';
                        } else {
                            echo '<p>Nikdo z komunity zatím nepublikoval žádný příspěvek. <a href="/zaregistrovat-se/" title="Zaregistrovat se">Zaregistruj se a začni publikovat!</a></p>';
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'latestnews');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>