<?php
/*
Template Name: Register
*/
?>

<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <?php
            if(have_posts()){
                while(have_posts()){
                    the_post();
                    echo '<h1>'.get_the_title().'</h1>';
                    echo '<div class="userprofiles__form userprofiles__registrationform">';
                    the_content();
                    echo '</div>';
                }
            }
        ?>
    </div>
</div>

<script type="text/javascript">
    /* Add mask options for password field */

    $(document).ready(function(){
        /* Get element */
        var input = $('input#user_password-371');
        /* Set desired position property */
        input.css({position:'relative'});
        /* Get data */
        var field = {
            element: input,
            dimension: input.outerHeight(),
            parent: input.parent()
        };
        /* Prepare parent element */
        field.parent.css('position','relative');
        /* Prepare icon */
        var trigger = $('<span style="position: absolute;" class="js-maskpsw"></span>');
        trigger.css({
            width:field.dimension+'px',
            height:field.dimension+'px',
            textAlign:'center',
            color:'#d0d0d0',
            transition:'300ms',
            top:0,
            right:'10px',
            fontSize:'21px'
        });
        /* Set hover state */
        trigger.hover(function(e){
            $(this).css({
                color:e.type === 'mouseenter' ? '#a0a0a0' : '#d0d0d0',
                cursor:'pointer'
            });
        });
        var icon = $('<i class="fa fa-eye"></i>').css('line-height',field.dimension + 'px');
        trigger.append(
            icon
        );
        /* Append element to parent */
        field.parent.append(trigger);
        /* Prepare handlers */
        trigger
            .mouseup(function(){
                input.attr('type', 'password');
            })
            .mousedown(function(){
                input.attr('type', 'text');
            });
        /* Create default handler */
        $(document).mouseup(function(){
            input.attr('type', 'password');
        });
    });
</script>

<?php
get_template_part('partials/footer/footer','main');
?>