<?php
/*
Template Name: Reset password
*/
?>

<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <?php
            if(have_posts()){
                while(have_posts()){
                    the_post();
                    echo '<h1>'.get_the_title().'</h1>';
                    the_content();
                }
            }
            ?>
            <style type="text/css">
                .um-field-block,
                .um-field-block > div{
                    text-align: left !important;
                }
                .um-field-block > div{
                    padding-bottom: 8px;
                }
                .um-field-area{
                    width: 50%;
                }
                .um-field-area input{
                    width: 100%;
                }
                .um-col-alt{
                    margin-top: 12px;
                }
            </style>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'latestnews');
                ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'community');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>