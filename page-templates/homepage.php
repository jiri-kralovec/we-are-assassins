<?php
/*
Template Name: Homepage
*/
?>

<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <!-- Featured posts  -->
    <div class="container">
        <section class="homepage__featuredposts">
            <div class="row">
                <?php
                $posts = array();
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3
                );
                $query = new WP_Query($args);
                if($query->have_posts()){
                    while($query->have_posts()){
                        $query->the_post();
                        $p = array(
                            'title'     => get_the_title(),
                            'date'      => get_the_date('j/n/Y g:i', $post->ID),
                            'author'    => get_the_author(),
                            'link'      => get_the_permalink(),
                            'thumb'     => get_the_post_thumbnail_url()
                        );
                        array_push($posts, $p);
                    }
                };
                ?>
                <div class="col-xs-12 col-md-8">
                <?php
                $data = $posts[0];
                echo '
                    <a href="'.$data["link"].'" title="'.$data["title"].'">
                        <div class="featured__post" style="background-image: url(\''.$data["thumb"].'\');">
                            <div class="loading-bar">
                                <div class="loading-bar-progress"></div>
                            </div>
                            <div class="content-wrapper">
                                <h1 class="title">'.$data["title"].'</h1>
                                <span class="author">'.$data["author"].'</span>
                                <span class="time">'.$data["date"].'</span>
                            </div>
                            <div class="gradient-mask"></div>
                        </div>
                    </a>
                ';
                ?>
                </div>
                <div class="col-xs-12 col-md-4">
                <?php
                foreach($posts as $post){
                echo '
                    <article class="featured__thumbnail js-featuredpost">
                        <h2>'.$post["title"].'</h2>
                        <span style="display: none;" data-title="'.$post["title"].'" data-image="'.$post["thumb"].'" data-author="'.$post["author"].'" data-posttime="'.$post["date"].'" data-permalink="'.$post["link"].'"></span>
                        <img style="display: none;" src="'.$post["thumb"].'">
                    </article>
                ';
                }
                ?>
                </div>
            </div>
        </section>
        <div class="col-xs-12">
            <?php
            get_template_part('partials/feeds/feed', 'community');
            ?>
        </div>
        <?php
        get_template_part('partials/feeds/feed','homepage-highlighted');
        ?>
    </div>  

    <!-- Static content -->
    <?php
    get_template_part('partials/statics/static','banner');
    ?>

    <div class="container">
        <section class="homepage__remainingposts">
            <div class="row">
                <div class="col-xs-12 last-xs col-md-8 first-md">
                    <div class="feed feed--official feed--active">
                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 6,
                        'offset' => 6
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()){
                        while($query->have_posts()){
                            $query->the_post();
                            
                            $posted = get_the_date('j/n/Y g:i', $post->ID);
                            $excerpt = substr(get_the_content(), 0, 200) . '... <a href="'.get_the_permalink().'" title="'.get_the_title().'">Read more</a>';

                            echo '
                            <article class="postfeed__post">
                                <div class="row">
                                    <div class="col-sm-3 col-md-4 hidden-xs">
                                        <a class="postfeed__postthumbnail" href="'.get_the_permalink().'" title="'.get_the_title().'" style="background-image: url(\''.get_the_post_thumbnail_url().'\');"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-9 col-md-8">
                                        <a class="postfeed__postheadline" href="'.get_the_permalink().'" title="'.get_the_title().'">
                                            <h2>'.get_the_title().'</h2>
                                        </a>
                                        <span class="postfeed__postauthor">'.get_the_author().'</span>
                                        <span class="postfeed__posttime">'.$posted.'</span>
                                        <p class="postfeed__postexcerpt">'.$excerpt.'</p>
                                    </div>
                                </div>
                            </article>
                            ';
                        }
                    }
                    ?>
                    </div>
                    <div class="feed feed--community community-feed">
                        <div class="row">
                        <?php

                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $args = array(
                            'post_type'     => array('waa_post','waa_imagepost'),
                            'posts_per_page'=> 6
                        );
                        $query = new WP_Query($args);
                        if($query->have_posts()){
                            while($query->have_posts()){
                                $query->the_post();
                                $posted = get_the_date('j/n/Y g:i', $post->ID);
                                $content;
                                
                                um_fetch_user(get_the_author_meta('ID'));

                                $avatarUrl = um_get_user_avatar_url();
                    
                                switch(get_post_type()){
                                    case 'waa_post':
                                        $content = '<p class="post__plain">'.substr(get_the_content(), 0, 200) . '...</p>';
                                        break;
                                    case 'waa_imagepost':
                                        $content = get_the_content();
                                }
                    
                                echo '
                                    <div class="col-xs-12 col-md-6">
                                        <a href="'.get_the_permalink().'" title="Posted by '.get_the_author().'">
                                            <article class="post">
                                                <div class="post__userimage">
                                                    <img class="post__avatar" src="'.$avatarUrl.'" alt="'.get_the_author().'">
                                                </div>
                                                <div class="post__body">
                                                    <span class="post__content">'.$content.'</span>
                                                    <span class="post__date">'.$posted.'</span>
                                                </div>
                                            </article>
                                        </a>   
                                    </div>
                                ';
                            }
                        } else {
                            if(is_user_logged_in()){
                                echo '<p>There are no community posts yet. <a href="/novy-prispevek/" title="Add post">Add your post now!</a></p>';
                            } else {
                                echo '<p>There are no community posts yet. <a href="/zaregistrovat-se/" title="Register now!">Register and start writing!</a></p>';
                            }
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <ul class="sidebar--homepage">
                        <li class="sidebar__item js-homepageremposts active" data-bind="feed--official">
                            <i class="fa fa-heart"></i> Posted by We Are Assassins
                        </li>
                        <li class="sidebar__item js-homepageremposts" data-bind="feed--community">
                            <i class="fa fa-users"></i> Posted by Community
                        </li>
                    </ul>   
                </div> 
            </div>
        </section>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>