<?php
/* Custom variables and settings */
$settingsId = 274;

/* Menus */
register_nav_menus(array(
    'header_menu'           => 'Header Main Menu',
    'header_menu_mobile'    => 'Header Mobile Menu',
    'footer_menu'           => 'Footer menu',
    'social_media'   => 'Social Media'
));

/* Thumbnails */
add_theme_support( 'post-thumbnails' ); 

/* Custom functions */
function get_page_pagination($query, $labels = null){
    $labelNew = ($labels === null) ? 'Newer posts' : $labels['newer'];
    $labelOld = ($labels === null) ? 'Older posts' : $labels['older'];
    echo '
    <section class="postfeed__pagination">
        '.get_previous_posts_link($labelNew, $query->max_num_pages).'
        '.get_next_posts_link($labelOld, $query->max_num_pages).'
    </section>
    ';
}

/* Register user posts */   
class waa_post {
    function waa_post() {
        add_action('init',array($this,'create_post_type'));
    }
    function create_post_type() {
        $labels = array(
            'name' => 'Posts',
            'singular_name' => 'Post',
            'add_new' => 'New post',
            'all_items' => 'All posts',
            'add_new_item' => 'Add new post',
            'edit_item' => 'Edit post',
            'new_item' => 'New post',
            'view_item' => 'View post',
            'search_items' => 'Search posts',
            'not_found' =>  'No posts found',
            'not_found_in_trash' => 'No deleted posts',
            'parent_item_colon' => 'No parent posts',
            'menu_name' => 'Community posts'
        );
        $args = array(
            'labels' => $labels,
            'description' => "Community posts",
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'menu_icon' => 'null',
            'capability_type' => 'post',
            'hierarchical' => true,
            'supports' => array('title','editor'),
            'has_archive' => true,
            'rewrite' => array('slug' => 'komunitni-prispevky'),
            'query_var' => true,
            'can_export' => true
        );
        register_post_type('waa_post',$args);
    }
}        
class waa_imagepost {

    function waa_imagepost() {
        add_action('init',array($this,'create_post_type'));
    }

    function create_post_type() {
        $labels = array(
            'name' => 'Posts',
            'singular_name' => 'Post',
            'add_new' => 'New image',
            'all_items' => 'All posts',
            'add_new_item' => 'Add new post',
            'edit_item' => 'Edit post',
            'new_item' => 'New image',
            'view_item' => 'View post',
            'search_items' => 'Search posts',
            'not_found' =>  'No posts found',
            'not_found_in_trash' => 'No deleted posts',
            'parent_item_colon' => 'No parent posts',
            'menu_name' => 'Community image posts'
        );
        $args = array(
            'labels' => $labels,
            'description' => "Community image posts",
            'public' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'menu_icon' => 'null',
            'capability_type' => 'post',
            'hierarchical' => true,
            'supports' => array('title','editor'),
            'has_archive' => true,
            'rewrite' => array('slug' => 'komunitni-obrazky'),
            'query_var' => true,
            'can_export' => true
        );
        register_post_type('waa_imagepost',$args);
    }
}

$waa_post = new waa_post();
$waa_imagepost = new waa_imagepost();

/* Creating custom actions */
												
?>