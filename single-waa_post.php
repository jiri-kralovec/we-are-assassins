
<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <?php
            if(have_posts()){
                while(have_posts()){
                    the_post();
                    echo '<h1>'.get_the_title().'</h1>';
                    the_content();
                }
            }
            ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'latestnews');
                ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'community');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>