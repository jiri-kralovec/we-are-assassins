var gulp         = require('gulp');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minify       = require('gulp-clean-css');
var images       = require('gulp-image');

var concat       = require('gulp-concat');
var rename       = require('gulp-rename');
var uglify       = require('gulp-uglify');

/* Styles */
var paths = {
  styles: {
    input: 'src/sass/default.scss',
    output: 'dist/'
  },
  scripts: {
    input: 'src/js/**/*.js',
    output: 'dist/'
  },
  images: {
    input: 'src/assets/*.{gif,jpg,png,svg}',
    output: 'dist/assets/images'
  },
  libraries: {
    input: 'src/libraries/*.{js,css}',
    output: 'dist/'
  }
}
var watch = {
  styles: 'src/sass/**/*.scss',
  scripts: 'src/js/**/*.js'
}
var settings = {
  sassOptions: {
    errLogToConsole: true,
    outputStyle: 'expanded'
  }
}

gulp.task('sass', function(){
  gulp.src(paths.styles.input)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(minify({compatibility: 'ie8'}))
    .pipe(gulp.dest(paths.styles.output))
});
gulp.task('scripts', function(){
  gulp.src(paths.scripts.input)
    .pipe(concat('application.js'))
    .pipe(gulp.dest(paths.scripts.output))
    .pipe(rename('application.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.output));
});
gulp.task('build-images', function() {
  gulp.src(paths.images.input).pipe(gulp.dest(paths.images.output));
});
gulp.task('build-libraries', function() {
  gulp.src(paths.libraries.input).pipe(gulp.dest(paths.libraries.output));
});

/* Init */
gulp.task('default', ['build-images', 'sass', 'scripts'], function(){
  gulp.watch(watch.styles, ['sass']);
  gulp.watch(watch.scripts, ['scripts']);
});