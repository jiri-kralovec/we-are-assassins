<?php
$settingsId = 274;
$bannerData = array(
    'bck' => get_field('static_banner', $settingsId)['background_image'],
    'img' => get_field('static_banner', $settingsId)['inline_image'],
    'col' => get_field('static_banner', $settingsId)['button_color_hex'],
    'link' => get_field('static_banner', $settingsId)['link'],
);
?>

<a href="<?php echo $bannerData['link']; ?>" title="View more">
    <section class="homepage__staticBlock" style="background-image: url('<?php echo $bannerData['bck']; ?>');">
        <div class="container">
            <div class="row" style="align-items: center;">
                <div class="col-xs-8 col-md-10 banner__content">
                    <img src="<?php echo $bannerData['img']; ?>" alt="Advertisment">
                </div>
                <div class="col-xs-4 col-md-2 banner__button">
                    <span class="btn btn--default" style="--styleCol: <?php echo '#'.$bannerData['col']; ?>;">
                    View more
                    </span>
                </div>
            </div>
        </div>
    </section>
</a>