<?php
$settingsId = 274;
$bannerData = array(
    'bck' => get_field('static_banner', $settingsId)['background_image'],
    'img' => get_field('static_banner', $settingsId)['inline_image'],
    'col' => get_field('static_banner', $settingsId)['button_color_hex'],
    'link' => get_field('static_banner', $settingsId)['link'],
);
?>

<a href="<?php echo $bannerData['link']; ?>" title="Zobrazit více">
    <section class="homepage__staticBlock" style="background-image: url('<?php echo $bannerData['bck']; ?>');">
        <div class="container">
            <div class="row" style="align-items: center;">
                <div class="col-xs-8 col-md-10">
                    <img src="<?php echo $bannerData['img']; ?>" alt="Zobrazit více">
                </div>
                <div class="col-xs-4 col-md-2">
                    <span class="btn btn--default" style="border-color: <?php echo $bannerData['col']; ?>; color: <?php echo $bannerData['col']; ?>">
                    Zobrazit více
                    </span>
                </div>
            </div>
        </div>
    </section>
</a>