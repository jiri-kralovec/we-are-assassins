<div class="offcanvas offcanvas__profile">
    <header>
        <div class="row">
            <div class="col-xs-7">
            <?php
            if(is_user_logged_in()){
                echo '<h2 class="offcanvas__headline">Profile</h2>';
            } else{
                echo '<h2 class="offcanvas__headline">Log in</h2>';
            }
            ?>
            </div>
            <div class="col-xs-offset-2 col-xs-3">
                <span class="offcanvas__controls js-trigger" data-target="offcanvas__profile">
                    <i class="fa fa-times"></i>
                <span>
            </div>
            <div class="col-xs-12">
                <div class="offcanvas__scrollable">
                    <div class="offcanvas__user">
                    <?php
                    if(is_user_logged_in()){
                        $user = wp_get_current_user();
                        $avatarUrl = get_avatar_url($user->ID);
                        $name = strlen(utf8_decode($user->display_name)) > 0 ? $user->display_name : $user->user_login;
                        /* Header */
                        echo '
                            <header>
                                <img class="user__avatar" alt="'.$user->display_name.'" src="'.$avatarUrl.'">
                                <h2 class="user__name">'.$name.'</h2>
                            </header>
                        ';
                        /* Buttons */
                        echo '
                            <a class="btn btn--default btn--primary btn--offcanvas" title="View profile" href="/profil/">View profile</a>
                            <a class="btn btn--default btn--secondary btn--offcanvas" title="Log out" href="/odhlasit-se/">Log out</a>
                        ';
                    } else {
                        echo do_shortcode('[ultimatemember form_id=374]');
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>