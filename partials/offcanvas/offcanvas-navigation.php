<div class="offcanvas offcanvas__navigation">
    <header>
        <div class="row">
            <div class="col-xs-7">
                <img class="offcanvas__logo" src="<?php echo get_template_directory_uri() . "/dist/assets/images/logo-lionhead-blue.png"; ?>" alt="We Are Assassins">
            </div>
            <div class="col-xs-offset-2 col-xs-3">
                <span class="offcanvas__controls js-trigger" data-target="offcanvas__navigation">
                    <i class="fa fa-times"></i>
                <span>
            </div>
        </div>
    </header>
    <div class="offcanvas__scrollable">
        <div class="offcanvas__nav">
        <?php
            $args = array(
                'theme_location' => 'header_menu',
                'menu_class' => 'header__menu'
            );
            wp_nav_menu($args);
        ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        /* Remove empty placeholder links */
        $(".offcanvas__nav ul li").each(function(){
            var link = $(this).find('a').eq(0);
            if(link.attr('href') == 'http://empty'){
                link.removeAttr('href');
            };
        });
        /* Handle submenu */
        $('li.menu-item-has-children a').on('click', function(e){
            $(this).parent().find('ul').slideToggle(250);
        });
    });
</script>