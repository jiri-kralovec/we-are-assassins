<?php
$settingsId = 274;
$currentObject = get_queried_object_id();
?>
<?php
    $pageData = [
        'title'             => get_field('page_meta_title', $settingsId),
        'desc'              => get_field('page_meta_description', $settingsId),
        'logo'              => get_field('page_logo', $settingsId)['url'],
        'facebookOgImage'   => get_field('facebook_og_image', $settingsId)['url'],

        'advertisements'    => [
            'top'           => [
                'link'  => get_field('header_advertisement', $settingsId)['link'],
                'alt'   => get_field('header_advertisement', $settingsId)['alt'],
                'img'   => get_field('header_advertisement', $settingsId)['image']
            ]
        ],

        'customMeta' => [
            'title' => get_field('local_meta_title', $currentObject),
            'desc'  => get_field('local_meta_description', $currentObject)
        ]
    ];
?>

<!doctype html>
<html>
    <head>
        <?php
            global $page, $paged;
            $pagefTitle = ($pageData['customMeta']['title'] === null) ? wp_title('|', false, 'right') . $pageData['title'] : $pageData['customMeta']['title'];
            $pagefDesc = ($pageData['customMeta']['desc'] === null) ? $pageData['desc'] : $pageData['customMeta']['desc'];
        ?>
        <!--
        Google Analytics Code
        -->
        <meta charset="utf-8">
        <meta name="author" content="<?php echo $pageData['title']; ?>">
        <meta name="description" content="<?php echo $pagefDesc; ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>
            <?php
                echo $pagefTitle;
            ?>
        </title>

        <!-- Facebook Opengraph -->
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo $pageData['title']; ?>" />
        <meta property="og:title" content="<?php echo $pagefDesc; ?>" />
        <meta property="og:description" content="<?php echo $pagefDesc; ?>" />
        <meta property="og:locale" content="cs" />
        <meta property="og:url" content="http://www.weareassassins.cz/" />
        
        <meta property="og:image" content="<?php echo $pageData['facebookOgImage']; ?>" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />
        <meta property="og:image:alt" content="<?php echo $pagefDesc; ?>" />

        <!-- Icons -->
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-16.png'; ?>" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-32.png'; ?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-96.png'; ?>" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-128.png'; ?>" sizes="128x128">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-160.png'; ?>" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-196.png'; ?>" sizes="196x196">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-favicon-228.png'; ?>" sizes="228x228">
         
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-57.png'; ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-60.png'; ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-72.png'; ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-76.png'; ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-114.png'; ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-120.png'; ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-144.png'; ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() . '/dist/assets/images/icon-appletouch-152.png'; ?>">

        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/dist/application.min.js'; ?>"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/solid.css" integrity="sha384-Rw5qeepMFvJVEZdSo1nDQD5B6wX0m7c5Z/pLNvjkB14W6Yki1hKbSEQaX9ffUbWe" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/brands.css" integrity="sha384-VGCZwiSnlHXYDojsRqeMn3IVvdzTx5JEuHgqZ3bYLCLUBV8rvihHApoA1Aso2TZA" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css" integrity="sha384-GVa9GOgVQgOk+TNYXu7S/InPTfSDTtBalSgkgqQ7sCik56N9ztlkoTr2f/T44oKV" crossorigin="anonymous">

        <link rel="stylesheet" href="https://use.typekit.net/orv4nsw.css">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:600,800&amp;subset=latin-ext" rel="stylesheet">

        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/dist/flexboxgrid.min.css'; ?>">        
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/dist/default.css'; ?>">
    </head>
    <body>
        <section class="page__topbar hidden--mobile">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                    <?php
                    $args = array(
                        'theme_location' => 'social_media',
                        'menu_class' => 'topbar__menu'
                    );
                    wp_nav_menu($args);
                    ?>
                    </div>
                    <div class="col-xs-6">
                    <?php
                    if(is_user_logged_in()){
                        $user = wp_get_current_user();
                        $name = strlen(utf8_decode($user->display_name)) > 0 ? $user->display_name : $user->user_login;
                        echo 'Logged in as <a href="/profil/" title="Log in">'.$name.'</a>';
                        echo '<a href="/odhlasit-se/" title="Log out" style="padding-left: 20px;">Log out</a>';
                    } else {   
                        echo '<a style="padding-right: 20px;" href="/prihlasit-se/" title="Log in">Log in</a>';
                        echo '<a href="/zaregistrovat-se/" title="Register now!">Register now!</a>';
                    }
                    ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="page__header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4">
                        <a href="/index.php" title="<?php echo $pageData['title']; ?>">
                            <img src="<?php echo $pageData['logo']; ?>" alt="<?php echo $pageData['desc']; ?>">
                        </a>
                    </div>
                    <div class="col-xs-8">
                        <div class="navigation--desktop">
                        <?php
                        $args = array(
                            'theme_location' => 'header_menu',
                            'menu_class' => 'header__menu'
                        );
                        wp_nav_menu($args);
                        ?>
                        </div>
                        <div class="navigation--mobile">
                            <span class="js-trigger nav__trigger" data-target="offcanvas__navigation">
                                <i class="fa fa-bars"></i>
                            </span>
                            <span class="js-trigger nav__trigger" data-target="offcanvas__profile">
                                <i class="fa fa-user-alt"></i>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
        <section class="page__advertisement">
            <div class="container">
                <a class="topadvertisement__element" style="background-image: url('<?php echo $pageData['advertisements']['top']['img']; ?>');" href="<?php echo $pageData['advertisements']['top']['link']; ?>" title="<?php echo $pageData['advertisements']['top']['alt']; ?>"></a>
            </div>
        </section>