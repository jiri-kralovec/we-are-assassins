<?php
$errorStr = '';
$hasError = false;
$confStr = '';

$user = wp_get_current_user()->ID;

function validateOldPassword($userId, $input){
    $usr = get_user_by('id', $userId);
    if($usr && wp_check_password($input, $usr->data->user_pass, $usr->ID)){
        return true;
    } else {
        return false;
    }
}

function validateOptions($prev,$new,$rpt,$user){
    if(validateOldPassword($user, $prev) == false){
        return '<span class="form__error">The current password does not match.</span>';
    }
    if(strlen($new) < 8){
        return '<span class="form__error">The new password has to be at least 8 characters long.</span>';
    }
    if($prev == $new){
        return '<span class="form__error">The new password cannot match the current one.</span>';
    }
    if($new !== $rpt){
        return '<span class="form__error">Password does not match the confirm password.</span>';
    }

    return true;
}

if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "change_password"){
    $form = array();
    $previousPass = '';
    $newPass = '';
    $rptPass = '';

    /* check for empty fields */
    if(isset($_POST['psw_old']) && !empty($_POST['psw_old'])){
        $previousPass = $_POST['psw_old'];
    }
    if(isset($_POST['psw_new']) && !empty($_POST['psw_new'])){
        $newPass = $_POST['psw_new'];
    }
    if(isset($_POST['psw_rpt']) && !empty($_POST['psw_rpt'])){
        $rptPass = $_POST['psw_rpt'];
    }

    $validation = validateOptions($previousPass, $newPass, $rptPass, $user);
    
    if($validation === true){
        wp_set_password($newPass, $user);
        $validation = '<span class="form__error">The password has been successfully changed.</span>';
    } else {
        $hasError = true;
    }
}
?>

<form class="newpost__form" id="edituser" name="edituser" method="post" action="">
    <div class="form__errorblock"><?php echo ($hasError == true) ? $validation : ''; ?></div>
    <div class="form__confirmation"><?php echo ($hasError == true) ? '' : $validation; ?></div>
    <fieldset>
        <label for="psw_old">Current password</label>
        <input type="password" class="psw_old" id="psw_old" name="psw_old">
    </fieldset>
    <fieldset>
        <label for="psw_new">New password</label>
        <input type="password" class="psw_new" id="psw_new" name="psw_new">
    </fieldset>
    <fieldset>
        <label for="psw_rpt">Confirm new password</label>
        <input type="password" class="psw_rpt" id="psw_rpt" name="psw_rpt">
    </fieldset>
    <fieldset>
        <input type="submit" value="Save changes" tabindex="6" id="submit" name="submit" />
        <input type="hidden" name="action" value="change_password" />
        <?php wp_nonce_field('new-post'); ?>
    </fieldset>
</form>