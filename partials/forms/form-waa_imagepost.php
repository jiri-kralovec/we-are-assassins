<?php
$errorListI = '';
$formErrorI = false;
$confirmationI = '';

function getFileName($suffix){
    $hash = range(1000000, 9999999);
    $userId = get_current_user_id();
    $username = get_userdata($userId)->user_login;

    return 'img_'.$username.'_'.$hash.$suffix;
}

if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_image_post"){
    /* validation */
    if(isset($_POST['imagepost_title']) && !empty($_POST['imagepost_title'])){
        $titleI = $_POST['imagepost_title'];
    } else {
        $errorListI .= formError('The title of the post needs to be filled out.');
        $formErrorI = true;
    }
    if(!$_FILES || $_FILES['imagepost_content']['error'] > UPLOAD_ERR_OK){
        $errorListI .= formError('The image cannot stay empty.');
        $formErrorI = true;
    }


    if($formErrorI === false){
        /* Create post */
        $new_postI = array(
            'post_title' => $titleI,
            'post_status' => 'publish',
            'post_type' => 'waa_imagepost'
        );
        $pId = wp_insert_post($new_postI);
        if(!$pId){
            $errorListI = '<span class="form__error">The post was not created.</span>';
        }

        $attachmentUrl = null;

        /* Upload attachment to post and get url */
        foreach($_FILES as $file => $array){
            $filename = $array['name'];
            $filetype = basename($filename);
            $filedata = $array['tmp_name'];

            $upload_file = wp_upload_bits($filename, null, file_get_contents($filedata));
            if(!$upload_file['error']){
                $wp_filetype = wp_check_filetype($filetype, null);
                $attachment = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_parent' => $pId,
                    'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
                    'post_content' => '',
		            'post_status' => 'inherit'
                );
                $attachmentId = wp_insert_attachment($attachment, $upload_file['file'], $pId);
                $attachmentUrl = wp_get_attachment_url($attachmentId);
                if(!is_wp_error($attachmentId)){
                    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		            $attachment_data = wp_generate_attachment_metadata(attachmentId, $upload_file['file'] );
                    wp_update_attachment_metadata(attachmentId,  $attachment_data );
                }
            }

            break;
        }

        /* Alter post content */
        $post = array(
            'ID' => $pId,
            'post_content' => '<img src="'.$attachmentUrl.'" alt="'.$titleI.'">'
        );
        if(wp_update_post($post)){
            $confirmationI = '<span class="form__confirmationtext">Příspěvek byl úspěšně publikován</span>';
        }
    }
}
?>

<form class="newpost__form" id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data">
    <div class="form__errorblock"><?php echo $errorListI; ?></div>
    <div class="form__confirmation"><?php echo $confirmationI; ?></div>
    <fieldset>
        <label for="imagepost_title">Post title</label>
        <input type="text" class="newpost__posttitle" id="imagepost_title" name="imagepost_title">
    </fieldset>
    <fieldset>
        <label for="imagepost_content">Post content</label>
        <span class="filehandler">
            <div class="fileinput__mask js-fileinput" data-reference="imagepost_content"><i class="fa fa-upload"></i> Select file</div>
            <input type="file" id="imagepost_content" name="imagepost_content">
        </span>
    </fieldset>
    <fieldset>
        <input type="submit" value="Publish post" tabindex="6" id="submit" name="submit" />
        <input type="hidden" name="action" value="new_image_post" />
        <?php wp_nonce_field('new-post'); ?>
    </fieldset>
</form>