<?php
$errorList = '';
$formError = false;
$confirmation = '';

function formError($error){
    return '<span class="form__error">'.$error.'</span>';
}

if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_text_post"){
    /* validation */
    if(isset($_POST['textpost_title']) && !empty($_POST['textpost_title'])){
        $title = $_POST['textpost_title'];
    } else {
        $errorList .= formError('The title of the post needs to be filled out.');
        $formError = true;
    }
    if(isset($_POST['textpost_content']) && !empty($_POST['textpost_content'])){
        $content = $_POST['textpost_content'];
    } else {
        $errorList .= formError('The content of the post needs to be filled out.');
        $formError = true;
    }

    if($formError === false){
        $new_post = array(
            'post_title'    => $title,
            'post_content'  => $content,
            'post_status'   => 'publish',
            'post_type'     => 'waa_post'
        );

        $p = wp_insert_post($new_post);
        if(!$p){
            $errorList = '<span class="form__error">The post was not created.</span>';
        } else {
            $confirmation = '<span class="form__confirmationtext">The post was successfully created.</span>';
        }
    }
}
?>

<form class="newpost__form" id="new_post" name="new_post" method="post" action="">
    <div class="form__errorblock"><?php echo $errorList; ?></div>
    <div class="form__confirmation"><?php echo $confirmation; ?></div>
    <fieldset>
        <label for="textpost_title">Post title</label>
        <input type="text" class="newpost__posttitle" id="textpost_title" name="textpost_title">
    </fieldset>
    <fieldset>
        <label for="textpost_content">Post content</label>
        <textarea name="textpost_content" id="textpost_content" style="resize: none;"></textarea>
    </fieldset>
    <fieldset>
        <input type="submit" value="Publish post" tabindex="6" id="submit" name="submit" />
        <input type="hidden" name="action" value="new_text_post" />
        <?php wp_nonce_field('new-post'); ?>
    </fieldset>
</form>