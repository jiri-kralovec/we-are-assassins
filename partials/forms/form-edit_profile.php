<?php
$errorList = '';
$formError = false;
$confirmation = '';

function formError($error){
    return '<span class="form__error">'.$error.'</span>';
}

$user = wp_get_current_user()->ID;

if('POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "edit_profile"){
    $adjust = array();
    /* validation */
    if(isset($_POST['user_nickname'])){
        $title = $_POST['user_nickname'];
        array_push($adjust, array(
            'key' => 'nickname',
            'val' => $_POST['user_nickname']
        ));
    }
    if(isset($_POST['user_firstname'])){
        $title = $_POST['user_firstname'];
        array_push($adjust, array(
            'key' => 'first_name',
            'val' => $_POST['user_firstname']
        ));
    }
    if(isset($_POST['user_lastname'])){
        $title = $_POST['user_lastname'];
        array_push($adjust, array(
            'key' => 'last_name',
            'val' => $_POST['user_lastname']
        ));
    }
    if(isset($_POST['user_desc'])){
        $title = $_POST['user_desc'];
        array_push($adjust, array(
            'key' => 'description',
            'val' => $_POST['user_desc']
        ));
    }

    foreach($adjust as $row){
        update_user_meta($user, $row['key'], $row['val']);
    }
}
?>

<?php
    $currentValues = get_user_meta($user);
?>

<form class="newpost__form" id="edituser" name="edituser" method="post" action="">
    <div class="form__errorblock"><?php echo $errorList; ?></div>
    <div class="form__confirmation"><?php echo $confirmation; ?></div>
    <fieldset>
        <label for="user_nickname">Nickname</label>
        <input type="text" class="user_nickname" id="user_nickname" name="user_nickname" value="<?php echo $currentValues['nickname'][0]; ?>">
    </fieldset>
    <fieldset>
        <label for="user_firstname">First name</label>
        <input type="text" class="user_firstname" id="user_firstname" name="user_firstname" value="<?php echo $currentValues['first_name'][0]; ?>">
    </fieldset>
    <fieldset>
        <label for="user_lastname">Last name</label>
        <input type="text" class="user_lastname" id="user_lastname" name="user_lastname" value="<?php echo $currentValues['last_name'][0]; ?>">
    </fieldset>
    <fieldset>
        <label for="user_desc">Bio</label>
        <textarea name="user_desc" id="user_desc" style="resize: none;"><?php echo $currentValues['description'][0]; ?></textarea>
    </fieldset>
    <fieldset>
        <input type="submit" value="Save changes" tabindex="6" id="submit" name="submit" />
        <input type="hidden" name="action" value="edit_profile" />
        <?php wp_nonce_field('new-post'); ?>
    </fieldset>
</form>