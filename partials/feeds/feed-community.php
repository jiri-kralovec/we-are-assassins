<section class="page__widget widget--fullsize">
    <header>
        <h2>Posted by community</h2>
    </header>
    <section class="feed community-feed">
        <div class="row">
        <?php
        $args = array(
            'post_type'     => array('waa_post','waa_imagepost'),
            'posts_per_page'=> 3
        );
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                $posted = get_the_date('j/n/Y g:i', $post->ID);
                $content;
                
                um_fetch_user(get_the_author_meta('ID'));

                $avatarUrl = um_get_user_avatar_url();
    
                switch(get_post_type()){
                    case 'waa_post':
                        $content = '<p class="post__plain">'.substr(get_the_content(), 0, 200) . '...</p>';
                        break;
                    case 'waa_imagepost':
                        $content = get_the_content();
                }
    
                echo '
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="'.get_the_permalink().'" title="Posted by '.get_the_author().'">
                            <article class="post">
                                <div class="post__userimage">
                                    <img class="post__avatar" src="'.$avatarUrl.'" alt="'.get_the_author().'">
                                </div>
                                <div class="post__body">
                                    <span class="post__content">'.$content.'</span>
                                    <span class="post__date">'.$posted.'</span>
                                </div>
                            </article>
                        </a>   
                    </div>
                ';
            }
        } else {
            if(is_user_logged_in()){
                echo '<p>There are no community posts yet. <a href="/novy-prispevek/" title="Add post">Add your post now!</a></p>';
            } else {
                echo '<p>There are no community posts yet. <a href="/zaregistrovat-se/" title="Register now!">Register and start writing!</a></p>';
            }
        }
        ?>
        </div>
    </section>
</section>