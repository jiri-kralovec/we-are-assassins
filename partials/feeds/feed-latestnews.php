<section class="page__widget widget--fullsize">
    <header>
        <h2>Latest news</h2>
    </header>
    <section class="feed">
        <div class="row">
        <?php
        $args = array(
            'posts_per_page' => 3,
            'post_type' => 'post'
        );
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                $posted = get_the_date('j/n/Y g:i', $post->ID);

                echo '
                <article class="col-xs-12 col-sm-4">
                    <a href="'.get_the_permalink().'" title="'.get_the_title().'">
                        <div class="feed__post">
                            <h2 class="headline">'.get_the_title().'</h2>
                            <span class="author">'.get_the_author().'</span>
                            <span class="time">'.$posted.'</span>
                        </div>
                    </a>
                </article>
                ';
            }
        }
        ?>
        </div>
    </section>
</section>