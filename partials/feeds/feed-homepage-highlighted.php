<?php
$settingsId = 274;
?>

<section class="homepage__highlightedposts">
    <div class="row">
        <div class="col-xs-12">
            <header>
                <h2>Latest news</h2>
            </header>
        </div>
        <div class="col-xs-12 col-md-8">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'offset' => 3
        );
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                
                $posted = get_the_date('j/n/Y g:i', $post->ID);
                $excerpt = substr(get_the_content(), 0, 200) . '... <a href="'.get_the_permalink().'" title="'.get_the_title().'">Read more</a>';

                echo '
                <article class="postfeed__post">
                    <div class="row">
                        <div class="col-sm-3 col-md-4 hidden-xs">
                            <a class="postfeed__postthumbnail" href="'.get_the_permalink().'" title="'.get_the_title().'" style="background-image: url(\''.get_the_post_thumbnail_url().'\');"></a>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <a class="postfeed__postheadline" href="'.get_the_permalink().'" title="'.get_the_title().'">
                                <h2>'.get_the_title().'</h2>
                            </a>
                            <span class="postfeed__postauthor">'.get_the_author().'</span>
                            <span class="postfeed__posttime">'.$posted.'</span>
                            <p class="postfeed__postexcerpt">'.$excerpt.'</p>
                        </div>
                    </div>
                </article>
                ';
            }
        }
        ?>
        </div>
        <div class="col-xs-12 last-xs col-md-4 homepageAds">
            <?php
            $ad = array(
                'link' => get_field('homepage_advertisement', $settingsId)['link'],
                'mobile' => get_field('homepage_advertisement', $settingsId)['mobiletablet_image'],
                'desktop' => get_field('homepage_advertisement', $settingsId)['desktop_image'],
            );
            ?>
            <span class="hidden-mobile adDesktop">
                <a href="<?php echo $ad['link']; ?>" title="See more!">
                    <img src="<?php echo $ad['desktop']; ?>" alt="Advertisement">
                </a>
            </span>
            <span class="hidden-desktop adMobile">
                <a href="<?php echo $ad['link']; ?>" title="See more!">
                    <img src="<?php echo $ad['mobile']; ?>" alt="Advertisement">
                </a>
            </span>
        </div>
    </div>
    
    <section class="feed" style="display: none;">
        <div class="row">
        <?php
        $args = array(
            'posts_per_page' => 3,
            'post_type' => 'post'
        );
        $query = new WP_Query($args);
        if($query->have_posts()){
            while($query->have_posts()){
                $query->the_post();
                $posted = get_the_date('j/n/Y g:i', $post->ID);

                echo '
                <article class="col-xs-12 col-sm-4">
                    <a href="'.get_the_permalink().'" title="'.get_the_title().'">
                        <div class="feed__post">
                            <h2 class="headline">'.get_the_title().'</h2>
                            <span class="author">'.get_the_author().'</span>
                            <span class="time">'.$posted.'</span>
                        </div>
                    </a>
                </article>
                ';
            }
        }
        ?>
        </div>
    </section>
</section>