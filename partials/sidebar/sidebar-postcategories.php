<section class="page__sidebar sidebar__postcategories hidden-mobile">
    <header>
        <h3>Topics</h3>
    </header>
    <ul class="sidebar--default">
    <?php
    $categories = get_categories(array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'exclude' => array(1)
    ));
    foreach($categories as $category){
        echo '
        <li class="sidebar__item">
            <a href="'.get_category_link($category->cat_ID).'" title="'.$category->name.'">'.$category->name.'</a>
        </li>
        ';
    }
    ?>
    </ul>
</section>