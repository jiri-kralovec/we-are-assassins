<section class="page__sidebar sidebar__latestnews hidden-mobile">
    <header>
        <h3>Latest news</h3>
    </header>
    <ul class="sidebar--default">
    <?php
    global $post;
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 5
    );
    $query = new WP_Query($args);
    if($query->have_posts()){
        while($query->have_posts()){
            $query->the_post();
            $posted = get_the_date('j/n/Y', $post->ID);
            echo '
            <li class="sidebar__item sidebar__postfeed">
                <a href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <span class="sidebarfeed__posttitle">'.get_the_title().'</span>
                    <span class="sidebarfeed__postdate">Published on '.$posted.'</span>
                </a>
            </li>';
        }
    }
    ?>
    </ul>
</section>