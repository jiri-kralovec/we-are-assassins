<section class="page__sidebar sidebar__siblings">
    <header>
        <h3>Categories</h3>
    </header>
    <ul class="sidebar--default">
    <?php
    global $post;
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $post->post_parent,
        'orderby' => 'name',
        'order' => 'ASC'
    );
    $query = new WP_Query($args);
    if($query->have_posts()){
        while($query->have_posts()){
            $query->the_post();
            echo '
            <li class="sidebar__item">
                <a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a>
            </li>';
        }
    }
    ?>
    </ul>
</section>