<?php
$settingsId = 274;
?>
        <section class="page__newsletter">
            <div class="container">
                <div class="row middle-md">
                    <div class="col-xs-12 col-md-2" style="text-align: center;">
                        <img src="<?php echo get_template_directory_uri() . "/dist/assets/images/logo-lionhead.png"; ?>" alt="We Are Assassins">
                    </div>
                    <div class="col-xs-12 col-md-10">
                        <h2>Sign up for out newsletter!</h2>
                        <h3>Don't miss the latest news and events!</h3>
                        <?php
                        echo do_shortcode('[newsletter_form button_label="Sign up now"][newsletter_field name="email" label=""][/newsletter_form]');
                        ?>
                        <span class="newsletter__note">*by signing up to our newsletter, you agree to our privacy policy and personal data regulations</span>
                    </div>
                </div>
            </div>
        </section>

        <footer class="page__footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <header>
                            <h3>We Are Assassins</h3>
                        </header>
                        <p>
                        <?php
                            echo get_field('page_meta_description', $settingsId);
                        ?>
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-offset-3 col-md-2">
                        <header>
                            <h3>Navigation</h3>
                        </header>
                        <?php
                        $args = array(
                            'theme_location' => 'footer_menu',
                            'menu_class' => 'footer__menu'
                        );
                        wp_nav_menu($args);
                        ?>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <header>
                            <h3>Follow us</h3>
                        </header>
                        <ul class="footer__social-menu">
                            <li>
                                <a href="https://www.facebook.com/assassinscz/" title="Facebook">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/waaczsk/" title="Instagram">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>   
                    </div>
                </div>
            </div>
        </footer> 
    </body>
</html>