<?php
get_template_part('partials/header/header','main'); 
get_template_part('partials/offcanvas/offcanvas','navigation');
get_template_part('partials/offcanvas/offcanvas','profile');
?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <?php
            echo '
                <h1>Page not found</h1>
                <p>The page you are looking for does not exist. Please check the address and try again.</p>
            ';
            ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'latestnews');
                ?>
            </div>
            <div class="col-xs-12">
                <?php
                get_template_part('partials/feeds/feed', 'community');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('partials/footer/footer','main');
?>